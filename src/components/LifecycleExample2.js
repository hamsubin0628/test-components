import React, { useState, useEffect } from "react";

// 함수형 컴포넌트
function LifecycleExample2() {
    // useState( 값을 쓸거아 ) = 훅 => 함수
    // 수정인데 setCount로 이름을 지은 이유 => react에서 수정을 하려면 setState해야 하기 때문에
    // setCount 자리에 함수가 들어옴
    const [count, setCount] = useState(0);
    // 초기값 ( 리터럴 : 숫자, 부울린, 스트링 ), 그 값을 쓰겠다
    // 초기값을 준다는 것 => 값이 바뀔수도 있다 => 한 뭉탱이로 주니까 [count, setCount]

    // packing : 모으는 것
    // up packing : 푸는 것
    // return되는 값은 무조건 하나

    useEffect(() => {
        // 생성
        console.log('Component mounted');
        // 콘솔 로그 없어도 됨
        return () => {
            // 종료
            console.log('component will unmount');
        };
    }, []); // , 뒤에 아무것도 두지 않으면 => 제거
    // [] 감지할 ( 관리될 ) State( 값 )
    // return을 입력하면 죽음에 관련된 얘기
    
    // 무조건 실행됨
    // hook 없다가 null => 0 ( hook( use )이라서 )
    useEffect(() => {
        console.log('Count updated:', count);
    }, [count]);

    const handleIncrement = () => {
        setCount((prevCount) => prevCount + 1);
    };

    // 함수니까 return만 있으면 된다 ( render X )
    return (
        <div>
            <p>Count: {count}</p>
            <button onClick={handleIncrement}>Increment</button>
        </div>
    );
}

export default LifecycleExample2;