import React, { Component } from "react";

// 클래스 컴포넌트
class LifecycleExample extends Component {
    // 생성자, 가장 먼저 실행 되는 곳 => props를 받는다
    constructor(props) {
        super(props); // super =>  상속해준 사람을 찾아라 { Component } from 'react';
        // super 제일 먼저 줘야 함
        this.state = {
        // this. 여기 ( this라고 말하는 주체 constructor => constructor 입장에서 여기 ) => Component { 전체 } (컴파일 언어에서 이런식으로 찾아감)
            counter: 0, // 초기값이 0
        }; // key : value => json 혹은 map인데 js에서는 "객체" 라고 부른다
    } // state 없음
    // props ( 속성 ) = count
    // state = 0

    // 생성이라는 단계가 다 끝나면 componentDidMount 실행
    componentDidMount() {
        console.log('Component mounted');
    }

    // 수정이 없으면 componentDidUpdate 실행 X, 값 변경 됐을 시 실행
    // props => 프로퍼티 => 필요한 것, 컴파일 언어에서의 프로퍼티 : 속성, 클래스에서의 프로퍼티( 클래스의 속성 ) :
    componentDidUpdate(prevProps, prevState) {
        if (this.state.count !== prevProps.count) {
            // state.count( 현재 갖고 있는 값 ) 와 이전의 count가 같지 않으면
            console.log('Count updated:', this.state.count);
            // 'Count updated:' 현재 갖고 있는 값을 찍어라
            // prevProps와 prevState는 스냅샷에서 옴
        }
    }

    // 다른 페이지 호출 했을 시 componentWillUnmount 호출 됨
    componentWillUnmount() {
        console.log('Component Will unmount');
    }

    // 클래스 안에 있으니까 메서드라고 불러야 함
    // 클래스 : 멤버 메서드, 멤버 변수
    handleIncrement = () => {
        // 변수에 익명 함수
        this.setState((prevState) => ({count: prevState.count + 1})); // 이전 값에 그려라
        // setState를 안해주면 반영이 안됨
        // this.setState((prevState) => ({count: this.state.count + 1})); 이렇게 쓰면 나중에 문제가 생긴다 ( 1 Way Binding 이어서 )
        // this.setState((prevState) => ({count: this.state.count + 1}));
        // 함수를 호출한 순간 count로 5가 들어갔을 시 전부가 5가 들어감
    };


    render() {
        return (
            // 다 누군지 아니까 문제가 안된다
            <div>
                <p>Count: {this.state.count}</p>
                <button onClick={this.handleIncrement}>Increment</button>
            </div>
        );
    }
}

export default LifecycleExample;