import './App.css';
import Lifecycle from './components/Lifecycle/Lifecycle';
import LifecycleExample from "./components/LifecycleExample";

function App() {
  return (
      //   이거 지우면 안됨 <div>가 두 개 있어도 안됨 => return은 무조건 한 뭉탱이
      <div className="App">
        <LifecycleExample/>
      </div>
  );
}

export default App;
